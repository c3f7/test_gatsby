const themeOptions = require('gatsby-theme-apollo-docs/theme-options');

module.exports = {
  pathPrefix: '/docs',
  plugins: [
    {
      resolve: 'gatsby-theme-apollo-docs',
      options: {
        ...themeOptions,
        root: __dirname,
        subtitle: 'Apollo Basics',
        description: 'ABCD',
        spectrumPath: '/',
        sidebarCategories: {
          null: ['index'],
        },
      },
    },
  ],
};
